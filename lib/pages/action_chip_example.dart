import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ActionChipExample extends StatefulWidget {
  final String title;

  ActionChipExample(this.title);

  @override
  _ActionChipExampleState createState() => _ActionChipExampleState();
}

class _ActionChipExampleState extends State<ActionChipExample> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF7F6F3),
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: ActionChip(
          avatar: CircleAvatar(
            backgroundColor: Color(0xff5808e5),
            child: Icon(
              Icons.flutter_dash,
              color: Colors.white,
            ),
            radius: 16,
          ),
          label: Text('Say Hello'),
          labelStyle: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 14,
            color: Color(0xff5808e5),
          ),
          labelPadding: EdgeInsets.symmetric(
            vertical: 8.0,
            horizontal: 8.0,
          ),
          onPressed: () {
            Fluttertoast.showToast(msg: "Hello from ActionChip !");
          },
          backgroundColor: Colors.white,
          elevation: 8,
        ),
      ),
    );
  }
}
